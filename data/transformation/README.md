## Ablage für Daten- und Strukturmanipulation

Dieser Ordner dient der Ablage von Änderungen an den Datenstrukturen oder Änderungen.
Für jede neue Version gibt es einen entsprechenden Ordner, der ein Set von drei Dateien enthält, in denen alle Änderungen festgehalten werden.

1. *dev.sql* - Alle Statements während der Entwicklungszeit
2. *test.sql* - Alle Statements während der Testszeit
3. *bugfix.sql* - Alle Statements für die Behebung von Bugs der produktiven Version

TBC...